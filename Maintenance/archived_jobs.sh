heroku run bin/qgtunnel bundle exec rails runner "
  headers = [
    'Job Name',
    'Job Phase'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    FeaturedJob.where(is_archived: true).each do |table|
      csv << [
        table.name,
        table.job_phase,
      ]
    end
  end
puts csv_data" --app prod-relishcareers > ArchivedJobs.csv
