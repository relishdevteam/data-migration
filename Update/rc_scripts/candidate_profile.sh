heroku run bin/qgtunnel bundle exec rails runner "
  headers = [
    'Candidate Email',
    'Years of Work Experience',
    'US Work Authorization',

    'Gender',
    'Ethnicity',
    'Race',
    'Veteran Status',

    'Company Size Preference',
    'About Me',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    date_diff = (1.week.ago)..(Date.today.end_of_day)
    Student.where(updated_at: date_diff).each do |table|
      next if table.nil?
      profile = table.student_profile.nil? ? StudentProfile.new : table.student_profile
      cmpSizePref = ProfileOption::CompanySize.where(id: profile.company_size_preferences).pluck(:name)

      csv << [
        table.email,
        profile.experience,
        profile.us_work_authorization,

        profile.gender,
        profile.ethnicity,
        profile.race,
        profile.veteran_status,

        cmpSizePref,
        \"#{profile.description}\",
      ]
    end
  end
puts csv_data" --app prod-relishcareers > RC_Updated__CandidateProfile.csv
