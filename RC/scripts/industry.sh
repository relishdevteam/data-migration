heroku run bin/qgtunnel bundle exec rails runner "
  headers = [
    'Name', 'Description',
  ]

  data =
    ProfileOption::IndustryExperience.default_options |
    ProfileOption::IndustryPreference.default_options

  csv_data = CSV.generate do |csv|
    csv << headers
    data.each do |table|
      next if table.nil?
      csv << [
        table.name,
        nil
      ]
    end
  end
puts csv_data" --app prod-relishcareers > v1.0-Industry.csv

