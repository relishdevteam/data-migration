# Does password_digest = encrypted_password? 

# What is the provider? Is it the organization name?

# To get 'Account Confirmed' as a column, how can I use change_column on 'confirmed_at' to go 
# from datetime to boolean?

# heroku run bin/qgtunnel bundle exec rails runner
headers = [
    'First Name', 'Last Name', 'Email', 'Slug',
    'Password Digest', 'Provider', 'Reset Password Verification Code',
    'Profile Completion Score', 'Account Confirmation Code', 'Account Confirmed',
    'Deactivated', 'Deactivated At'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    User.all.each do |table|
      next if table.nil?
      org_name = Organization_Update.find_by(id: organization_id).description
      csv << [
        table.first_name, table.last_name, table.email, table.slug, table.encrypted_password,
        org_name, table.reset_password_token, nil, table.confirmation_token, table.???,
        table.???, table.deactivated_at
      ]
    end
  end
puts csv_data
# --app prod-relishcareers > rc_users_export.csv

# # Relish 2.0
#   create_table "users", force: :cascade do |t|
#     t.string   "email",                              default: "", null: false
#     t.string   "encrypted_password",                 default: "", null: false
#     t.string   "reset_password_token"
#     t.datetime "reset_password_sent_at"
#     t.datetime "remember_created_at"
#     t.integer  "sign_in_count",                      default: 0,  null: false
#     t.datetime "current_sign_in_at"
#     t.datetime "last_sign_in_at"
#     t.string   "current_sign_in_ip"
#     t.string   "last_sign_in_ip"
#     t.string   "role"
#     t.datetime "created_at"
#     t.datetime "updated_at"
#     t.string   "type"
#     t.string   "name"
#     t.integer  "organization_id"
#     t.string   "organization_type"
#     t.string   "slug"
#     t.string   "first_name"
#     t.string   "last_name"
#     t.string   "confirmation_token"
#     t.datetime "confirmed_at"
#     t.datetime "confirmation_sent_at"
#     t.string   "unconfirmed_email"
#     t.string   "generate_password"
#     t.string   "username"
#     t.string   "remember_token"
#     t.datetime "deactivated_at"
#     t.string   "permission_level"
#     t.string   "employer_name"
#     t.string   "unique_session_id",       limit: 20
#     t.string   "login_token"
#     t.jsonb    "notification_preference"
#   end


# # Relish 1.0
#   create_table "users", force: :cascade do |t|
#     t.string "first_name", default: "", null: false
#     t.string "last_name"
#     t.string "email", default: "", null: false
#     t.string "slug"
#     t.string "password_digest"
#     t.string "provider", default: "email"
#     t.integer "reset_password_verification_code"
#     t.integer "profile_completion_score", default: 0
#     t.integer "account_confirmation_code"
#     t.boolean "account_confirmed", default: false
#     t.boolean "deactivated", default: false
#     t.datetime "deactivated_at"
#     t.index ["email"], name: "index_users_on_email", unique: true
#     t.index ["slug"], name: "index_users_on_slug", unique: true
#   end