# This script joins multiple database tables to create a CSV export

# heroku run bundle exec rails runner
  headers = [
    'Company Name', 'Candidate Email',
    'Overall Satisfaction', 'Coworker Quality',
    'Advancement', 'Training Development',
    'Brand Prestige', 'Benefits Perks',
    'Firm Stability', 'Balance Flexibility',
    'Average Weekly Hours', 'Average Travel Percentage'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    CompanyPrestigeRating.each do |table|
      next if table.nil?
      company =
        Company.find_by(company_list_id: table.company_list_id) rescue Company.new
      csv << [
        company&.company_list&.name,
        company&.user&.email,
        company.overall_happiness,
        company.coworker_quality,
        company.advancement,
        company.training_development,
        company.brand_prestige,
        company.benefits_perks,
        company.firm_stability,
        company.balance_flexibility,
        company.hours_worked,
        company.travel_percent
      ]
    end
  end
puts csv_data
 --app komodo-production > WorkSatisfaction.csv

# SELECT DISTINCT
# companies.id AS work_experience_id,companies.overall_happiness AS overall_satisfaction,
# coworker_quality AS coworker_quality, advancement AS advancement,
# training_development AS training_development, brand_prestige AS brand_prestige,
# benefits_perks AS benefits_perks, firm_stability AS firm_stability,
# balance_flexibility AS balance_flexibility, company_prestige_ratings.created_at AS created_at,
# company_prestige_ratings.updated_at AS updated_at, companies.hours_worked AS average_weekly_hours,
# companies.travel_percent AS average_travel_percentage
# FROM companies
# INNER JOIN company_lists ON companies.company_list_id = company_lists.id
# INNER JOIN company_prestige_ratings ON company_prestige_ratings.company_list_id = company_lists.id
# INNER JOIN users ON companies.user_id = users.id

