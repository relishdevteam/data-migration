
heroku run bundle exec rails runner "
  headers = [
    'Candidate Email',
    'Years Of Work Experience',
    'USA Work Authorization',
    'Requires Visa Sponsorship',
    'Race',
    'Gender',
    'Ethnicity',
    'Company Size Preference',
    'Visible To Employers',
    'Citizenship',
    'Authorized Work Citizenship'
  ]
  date_diff = (1.week.ago)..(Date.today.end_of_day)
  csv_data = CSV.generate do |csv|
    csv << headers
    User.where(updated_at: date_diff).each do |table|
      next if table.nil?
      csv << [
        table.email,
        nil,
        table.usa_work_authorization,
        table.requires_visa_sponsorship,
        table.race,
        table.gender,
        table.ethnicity,
        nil,
        nil,
        nil,
        nil
      ]
    end
  end
puts csv_data" --app komodo-production > TC_Updated__CandidateProfile.csv
