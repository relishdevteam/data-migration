# This script joins multiple database tables to create a CSV export, including double joins

heroku run bundle exec rails runner "
  headers = [
    'Company Name',
    'Coworker Quality',
    'Advancement',
    'Training Development',
    'Brand Prestige',
    'Benefits Perks',
    'Firm Stability',
    'Balance Flexibility',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    CompanyValuePercentile.find_each do |obj|
      csv << [
        obj&.company_list&.name,
        (obj.coworker_quality * 100.0).round(0),
        (obj.advancement * 100.0).round(0),
        (obj.training_development * 100.0).round(0),
        (obj.brand_prestige * 100.0).round(0),
        (obj.benefits_perks * 100.0).round(0),
        (obj.firm_stability * 100.0).round(0),
        (obj.balance_flexibility * 100.0).round(0),
      ]
    end
  end
puts csv_data" --app komodo-production > TC_CompanyValuePercentile.csv
