heroku run bin/qgtunnel bundle exec rails runner "
  headers = [
    'Description',
    'Saved Employer ID',
    'Saved Event ID',
    'Saved Featured Job ID',
    'Saved Indeed Job ID',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Note.find_each do |table|
      next if table.nil?
      csv << [
        table.description, nil, nil, nil, nil,
      ]
    end
  end
puts csv_data" --app prod-relishcareers > Notes.csv
