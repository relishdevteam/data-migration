# This script joins multiple database tables to create a CSV export, including double joins

heroku run bundle exec rails runner "
  headers = [
    'Company Name',
    'Coworker Quality',
    'Advancement',
    'Training Development',
    'Brand Prestige',
    'Benefits Perks',
    'Firm Stability',
    'Balance Flexibility',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    CompanyValueAverageFinal.find_each do |obj|
      csv << [
        obj&.company_list&.name,
        obj.coworker_quality,
        obj.advancement,
        obj.training_development,
        obj.brand_prestige,
        obj.benefits_perks,
        obj.firm_stability,
        obj.balance_flexibility,
      ]
    end
  end
puts csv_data" --app komodo-production > TC_CompanyValueAverageFinal.csv


# count: attributes[:count],
# coworker_quality: attributes[:avg_coworker_quality].to_f,
# advancement: attributes[:avg_advancement].to_f,
# training_development: attributes[:avg_training_development].to_f,
# brand_prestige: attributes[:avg_brand_prestige].to_f,
# benefits_perks: attributes[:avg_benefits_perks].to_f,
# firm_stability: attributes[:avg_firm_stability].to_f,
# balance_flexibility: attributes[:avg_balance_flexibility].to_f