heroku run bundle exec rails runner "
  headers = [
    'Name',
    'Description',
    'Search Rank',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Position.find_each do |table|
      next if table.nil?
      csv << [
        table.position,
        nil,
        table.search_rank
      ]
    end
  end
puts csv_data" --app komodo-production > Position.csv