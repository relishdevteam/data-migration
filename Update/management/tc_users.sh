heroku run bundle exec rails runner "
  headers = [
    'First Name',
    'Last Name',
    'Email',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    User.find_each do |table|
      next if table.nil?
      csv << [
        table.first_name,
        table.last_name,
        table.email,
      ]
    end
  end
puts csv_data" --app komodo-production > TC_First_n_Last_names.csv
