# Where is the job description?

# heroku run bundle exec rails runner
headers = [
    'Company Name',
    'Name',
    'Job Level',
    'Experience Required',
    'Job Type',
    'Compensation Currency',
    'Apply Button Text',
    'Apply Button Link',
    'Show Expiration Date',
    'Job Phase'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    JobPost.all.each do |table|
      next if table.nil?
      csv << [
        table.company_list.name,
        table.title,
        nil,
        nil,
        table.job_type,
        nil,
        'Apply Job',
        table.external_link,
        nil,
        nil
      ]
    end
  end
puts csv_data
# --app komodo-production > FeaturedJob.csv

# Rel

  create_table "featured_jobs", force: :cascade do |t|
    t.bigint "company_id"
    t.string "name", null: false
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "job_level"
    t.string "experience_required"
    t.string "job_type", default: [], array: true
    t.string "compensation_currency"
    t.decimal "compensation_amount"
    t.string "apply_button_text", default: "Apply"
    t.string "apply_button_link"
    t.string "expiration_date"
    t.boolean "show_expiration_date", default: false
    t.string "job_phase", default: [], array: true
    t.index ["company_id"], name: "index_featured_jobs_on_company_id"
    t.index ["name"], name: "index_featured_jobs_on_name"
  end

## TC

  create_table "job_posts", id: :serial, force: :cascade do |t|
    t.string "title"
    t.integer "industry_id"
    t.integer "function_id"
    t.integer "city_id"
    t.integer "company_list_id"
    t.string "short_description"
    t.text "long_description"
    t.string "job_type"
    t.string "visa_sponsorship"
    t.datetime "apply_by"
    t.string "degree_type"
    t.integer "degree_id"
    t.string "class_year"
    t.text "additional_instructions"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "source"
    t.string "external_link"
    t.json "long_description_json"
    t.string "status"

