
heroku run bundle exec rails runner "
  headers = [
    'Name', 'Minimum Score',
    'Maximum Score', 'Sub Score Names',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    TestType.find_each do |table|
      next if table.nil?
      csv << [
        table.test_name,
        table.min_score,
        table.max_score,
        table.sub_score_names
      ]
    end
  end
puts csv_data" --app komodo-production > TestType.csv
