# I think Concentrations refers to names of Majors? Please confirm.

# heroku run bin/qgtunnel bundle exec rails runner
headers = [
    'Name'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Major.all.each do |table|
      next if table.nil?
      csv << [
        table.name
      ]
    end
  end
puts csv_data
# --app komodo-production > tc_concentration_export.csv

  # create_table "concentrations", force: :cascade do |t|
  #   t.string "name", null: false
  #   t.datetime "created_at", null: false
  #   t.datetime "updated_at", null: false
  # end
