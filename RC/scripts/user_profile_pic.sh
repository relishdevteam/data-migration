heroku run bin/qgtunnel bundle exec rails runner "
  headers = [
    'Email',
    'Profile Pic URL'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Image::ProfilePic.find_each do |table|
      next if table.nil?
      csv << [
        table&.imageable&.email,
        table&.avatar&.expiring_url(1.week)
      ]
    end
  end
puts csv_data" --app prod-relishcareers > UserProfilePic.csv
