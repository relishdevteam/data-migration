# Aren't candidates, not schools, saving employers? If so, use table.candidate_id in col2

# heroku run bin/qgtunnel bundle exec rails runner
headers = [
    'Profile ID', 'School ID', 'Created At', 'Updated At'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Saved_Employer.all.each do |table|
      next if table.nil?
      csv << [
        table.company_id, nil, table.created_at, table.updated_at
      ]
    end
  end
puts csv_data
# --app prod-relishcareers > saved_employers_export.csv

# # v1

#   create_table "saved_employers", force: :cascade do |t|
#     t.integer "candidate_id", null: false
#     t.integer "company_id", null: false
#     t.index ["candidate_id"], name: "index_saved_employers_on_candidate_id"
#     t.index ["company_id"], name: "index_saved_employers_on_company_id"
#   end

# # v2
#   create_table "save_employer_profiles", force: :cascade do |t|
#     t.integer  "profile_id"
#     t.integer  "school_id"
#     t.datetime "created_at", null: false
#     t.datetime "updated_at", null: false
#   end
