# Where is the job description?

heroku run bundle exec rails runner "
  headers = [
    'Company Name',
    'Name',
    'Job Level',
    'Experience Required',
    'Job Type',
    'Compensation Currency',
    'Apply Button Text',
    'Apply Button Link',
    'Show Expiration Date',
    'Job Phase'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    JobPost.each do |table|
      next if table.nil?
      csv << [
        table.company_list.name,
        table.title,
        nil,
        nil,
        table.job_type,
        nil,
        'Apply Job',
        table.external_link,
        nil,
        nil
      ]
    end
  end
puts csv_data" --app komodo-production > FeaturedJob.csv
