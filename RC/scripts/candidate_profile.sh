heroku run bin/qgtunnel bundle exec rails runner "
  headers = [
    'Candidate Email',
    'Years of Work Experience',
    'US Work Authorization',
    'Require Visa Sponsorship',

    'Gender',
    'Ethnicity',
    'Race',
    'Veteran Status',

  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Student.find_each(batch_size: 3000) do |table|
      next if table.nil?
      profile = table.student_profile.nil? ? StudentProfile.new : table.student_profile

      csv << [
        table.email,
        profile.experience,
        profile.us_work_authorization,
        profile.requires_visa_sponsorship,

        profile.gender,
        profile.ethnicity,
        profile.race,
        profile.veteran_status,

      ]
    end
  end
puts csv_data" --app prod-relishcareers > CandidateProfile.csv
