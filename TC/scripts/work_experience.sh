# This script joins multiple database tables to create a CSV export

heroku run bundle exec rails runner "
  headers = [
    'Candidate Email',
    'Company Name',

    'Title',
    'Job Type',
    'Visa Sponsorship',
    'Job Function',
    'Nearest City',
    'Start Month',
    'Start Year',
    'End Month',
    'End Year',

    'Compensation Type',
    'Currency Type',
    'Compensation Start Year',
    'Annualized Salary',
    'Performance Bonus',
    'Signing Bonus',
    'Relocation Bonus',
    'Annual Stock Compensation',
    'Misc Compensation',
    'Negotiation Result',
    'Hourly Wage',
    'Fixed Compensation Amount',
    'Fixed Compensation Interval',
    'Job Duration',
    'Equity Granted',
    'Keep Compensation Data Public In Charts',

    'Overall Satisfaction',
    'Coworker Quality',
    'Advancement',
    'Training Development',
    'Brand Prestige',
    'Benefits Perks',
    'Firm Stability',
    'Balance Flexibility',
    'Average Weekly Hours',
    'Average Travel Percentage'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Company.find_each do |table|
      next if table.nil?
      pos = table&.position&.position
      next unless pos.present?
      next unless table.reviewed
      next unless table.complete

      csv << [
        table&.user&.email,
        table&.company_list&.name,

        pos,
        table&.job_type,
        table&.visa_sponsorship,
        table&.function&.name,
        table&.city&.name,
        table&.start_month,
        table&.start_year,
        table&.end_month,
        table&.end_year,

        table&.compensation_type,
        table&.currency,
        table&.compensation_start_year,
        table&.salary,
        table&.bonus,
        table&.signing_bonus,
        table&.relocation_bonus,
        table&.stock_comp,
        table&.other_comp,
        table&.negotiation_result,
        table&.hourly_wage,
        table&.fixed_amount,
        table&.fixed_type,
        nil,
        table&.startup_equity,
        table&.public_sat_data,

        table&.overall_happiness,
        table&.coworker_quality,
        table&.advancement,
        table&.training_development,
        table&.brand_prestige,
        table&.benefits_perks,
        table&.firm_stability,
        table&.balance_flexibility,
        table&.hours_worked,
        table&.travel_percent,
      ]
    end
  end
puts csv_data" --app komodo-production > WorkExperience.csv
