
heroku run bundle exec rails runner "
  headers = [
    'Candidate Email',
    'Years Of Work Experience',
    'USA Work Authorization',
    'Requires Visa Sponsorship',

    'Race',
    'Gender',
    'Ethnicity',
    'Veteran Status',

    'Company Size Preference',
    'Visible To Employers',
    'Citizenship',
    'Authorized Work Citizenship',
    'About Me'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    User.find_each do |table|
      next if table.nil?
      csv << [
        table.email,
        nil,
        table.usa_work_authorization,
        table.requires_visa_sponsorship,
        table.race,
        table.gender,
        table.ethnicity,
        nil,
        nil,
        nil,
        nil,
        nil,
        nil
      ]
    end
  end
puts csv_data" --app komodo-production > CandidateProfile.csv
