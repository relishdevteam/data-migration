heroku run bin/qgtunnel bundle exec rails runner "
  headers = [
    'Company Name',
    'Name',
    'Calendar Date',
    'Attendance Status',
    'Slug',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Event.find_each do |table|
      next if table.nil?
      csv << [
        nil,
        table.name,
        table.event_date,
        nil,
        table.slug,
      ]
    end
  end
puts csv_data" --app prod-relishcareers > Events.csv
