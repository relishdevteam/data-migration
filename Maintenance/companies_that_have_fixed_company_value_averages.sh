# This script joins multiple database tables to create a CSV export, including double joins

heroku run bundle exec rails runner "
  headers = [
    'Name',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    CompanyValueSeededAverage.where(override: false).each do |obj|
      csv << [
        obj&.company_list&.name,
      ]
    end
  end
puts csv_data" --app komodo-production > CompaniesWithSeeedAverageValues.csv
