# This script joins multiple database tables to create a CSV export

# heroku run bin/qgtunnel bundle exec rails runner
headers = [
    'Company ID', 'Name', 'URL'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Company_Profile.all.each do |table|
      next if table.nil?
      name = Company.find_by(id: company_id).name
      csv << [
        table.company_id, name, table.website_url
      ]
    end
  end
puts csv_data
# --app prod-relishcareers > rc_company_quick_links_export.csv


# # v2
#   create_table "company_quick_links", force: :cascade do |t|
#     t.integer "company_id", null: false
#     t.string "name", default: "", null: false
#     t.string "url", default: "", null: false
#   end

# # v1
#   create_table "company_profiles", force: :cascade do |t|
#     t.string   "industry",                                          array: true
#     t.string   "location"
#     t.string   "location_type"
#     t.string   "latitude"
#     t.string   "longitude"
#     t.string   "position_available",      default: [],              array: true
#     t.string   "recruiting_presence"
#     t.string   "company_size"
#     t.integer  "company_id"
#     t.integer  "school_id"
#     t.datetime "created_at",                           null: false
#     t.datetime "updated_at",                           null: false
#     t.string   "website_url"
#     t.string   "career_url"
#     t.string   "linkedin_url"
#     t.string   "twitter_url"
#     t.string   "facebook_url"
#     t.text     "description"
#     t.string   "roles_available",         default: [],              array: true
#     t.string   "cities",                  default: [],              array: true
#     t.string   "states",                  default: [],              array: true
#     t.string   "visa_authorization"
#     t.string   "international_regions",   default: [],              array: true
#     t.string   "international_countries", default: [],              array: true
#     t.string   "employer_type"
#     t.integer  "certified_h1b_visas",     default: 0
#   end