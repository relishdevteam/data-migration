heroku run bundle exec rails runner "
  headers = [
    'School', 'Start Year',
    'End Year', 'Program Type',
    'Concentration', 'Candidate Email',
    'GPA Score', 'Degree'
  ]
  date_diff = (1.week.ago)..(Date.today.end_of_day)
  csv_data = CSV.generate do |csv|
    csv << headers
    Education.where(updated_at: date_diff).each do |table|
      next if table.nil?
      csv << [
        table&.school&.name,
        table.start_year,
        table.class_year,
        table.program,
        table&.major&.name,
        table&.user&.email,
        table.gpa,
        table&.degree&.degree_name,
      ]
    end
  end
puts csv_data" --app komodo-production > TC_Updated__Education.csv
