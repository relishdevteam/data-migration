# This script joins multiple database tables to create a CSV export, including double joins

heroku run bundle exec rails runner "
  headers = [
    'Company Name',
    'Coworker Quality',
    'Advancement',
    'Training Development',
    'Brand Prestige',
    'Benefits Perks',
    'Firm Stability',
    'Balance Flexibility',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    CompanyValuePercentile.find_each do |obj|
      csv << [
        obj&.company&.name,
        obj.coworker_quality.round(0),
        obj.advancement.round(0),
        obj.training_development.round(0),
        obj.brand_prestige.round(0),
        obj.benefits_perks.round(0),
        obj.firm_stability.round(0),
        obj.balance_flexibility.round(0),
      ]
    end
  end
puts csv_data" --app production-relishcareers > RC_CompanyValuePercentile.csv
