# This script joins multiple database tables to create a CSV export

heroku run bundle exec rails runner "
  headers = [
    'Candidate Email',
    'Name',
    'Composite Score',
    'Sub Scores',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    TestScore.find_each do |table|
      next if table.nil?
      csv << [
        table&.user&.email,
        table.test_name,
        table.composite_score,
        table.sub_scores,
      ]
    end
  end
puts csv_data" --app komodo-production > TestScore.csv
