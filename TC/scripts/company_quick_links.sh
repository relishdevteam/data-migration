
heroku run bundle exec rails runner "
  headers = [
    'Company Name', 'Website URL'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    CompanyList.find_each do |table|
      next if table.nil?
      next unless table.domain.present?
      csv << [
        table.name,
        table.domain
      ]
    end
  end
puts csv_data" --app komodo-production > CompanyQuickLinks.csv
