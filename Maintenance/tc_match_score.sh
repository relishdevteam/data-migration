# This script joins multiple database tables to create a CSV export, including double joins

heroku run bundle exec rails runner "
  headers = [
    'Company Name',
    'Match Score value',
    'User - Coworker Quality',
    'User - Advancement',
    'User - Training Development',
    'User - Brand Prestige',
    'User - Benefits Perks',
    'User - Firm Stability',
    'User - Balance Flexibility',
  ]

  user = User.find_by(email: 'marvinjameskang@gmail.com')
  pref = user.job_value_preference

  value_params = %i(coworker_quality advancement training_development brand_prestige
                       benefits_perks firm_stability balance_flexibility)

  values = pref.attributes.symbolize_keys.slice(*value_params).transform_values { |s| (s*100.0).to_i }

  csv_data = CSV.generate do |csv|
    csv << headers
    CompanyList.find_each do |obj|
      result = MatchScoreCalculator.new(pref).normalized_for_employer(obj.id)
      next if result.match_score.nil?
      csv << [
        obj&.name,
        result.match_score,
        values[:coworker_quality],
        values[:advancement],
        values[:training_development],
        values[:brand_prestige],
        values[:benefits_perks],
        values[:firm_stability],
        values[:balance_flexibility],
      ]
    end
  end
puts csv_data" --app komodo-production > TC_User_MatchScore.csv


# count: attributes[:count],
# coworker_quality: attributes[:avg_coworker_quality].to_f,
# advancement: attributes[:avg_advancement].to_f,
# training_development: attributes[:avg_training_development].to_f,
# brand_prestige: attributes[:avg_brand_prestige].to_f,
# benefits_perks: attributes[:avg_benefits_perks].to_f,
# firm_stability: attributes[:avg_firm_stability].to_f,
# balance_flexibility: attributes[:avg_balance_flexibility].to_f