heroku run bin/qgtunnel bundle exec rails runner "
  headers = [
    'First Name',
    'Last Name',
    'Email',
    'Slug',
    'Password Digest',
    'Provider',
    'Reset Password Verification Code',
    'Profile Completion Score',
    'Account Confirmation Code',
    'Account Confirmed',
    'Deactivated',
    'Deactivated At'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Student.find_each do |table|
      next if table.nil?
      csv << [
        table.first_name,
        table.last_name,
        table.email,
        nil,
        nil,
        'email',
        nil,
        nil,
        nil,
        true,
        false,
        nil
      ]
    end
  end
puts csv_data" --app prod-relishcareers > User.csv
