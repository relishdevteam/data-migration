
# heroku run bin/qgtunnel bundle exec rails runner
headers = [
    'Work Experience ID', 'Title', 'Job Type', 'Visa Sponsorship',
    'Job Function', 'Nearest City', 'Start Month', 'Start Year',
    'End Month', 'End Year', 'Created At', 'Updated At'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Friendly_Id_Slug.all.each do |table|
      next if table.nil?
      csv << [
        table.slug, table.sluggable_id, table.sluggable_type, table.scope, table.created_at
      ]
    end
  end
puts csv_data
# --app prod-relishcareers > tc_friendly_id_slug_export.csv

## Rel

#   create_table "friendly_id_slugs", id: :serial, force: :cascade do |t|
#     t.string "slug", null: false
#     t.integer "sluggable_id", null: false
#     t.string "sluggable_type", limit: 50
#     t.string "scope"
#     t.datetime "created_at"


# ## TC

#   create_table "friendly_id_slugs", id: :serial, force: :cascade do |t|
#     t.string "slug", null: false
#     t.integer "sluggable_id", null: false
#     t.string "sluggable_type", limit: 50
#     t.string "scope"
#     t.datetime "created_at"