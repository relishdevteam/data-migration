heroku run bin/qgtunnel bundle exec rails runner "
  headers = [
    'Email',
    'Resume URL'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    date_diff = (1.week.ago)..(Date.today.end_of_day)
    Document::Resume.where(updated_at: date_diff).each do |table|
      next if table.nil?
      csv << [
        table&.documentable&.email,
        table&.record&.expiring_url(1.week)
      ]
    end
  end
puts csv_data" --app prod-relishcareers > RC_Updated__UserResume.csv
