heroku run bin/qgtunnel bundle exec rails runner "
  headers = [
    'Candidate Email',

    'Industry Experience',
    'Industry Preference',
    'Job Function Experience',
    'Job Function Preference',

    'Geographical Preference',
    'US Cities Preference',
    'US States Preference',
    'US Regions Preference',
    'Country Preference',
    'International Regions Preference',

  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Student.find_each do |table|
      next if table.nil?
      profile = table.student_profile.nil? ? StudentProfile.new : table.student_profile

      industryExpr = ProfileOption::IndustryExperience.where(id: profile.industry_experiences, default_profile_option: true).pluck(:name).uniq
      industryPref = ProfileOption::IndustryPreference.where(id: profile.industry_preferences, default_profile_option: true).pluck(:name).uniq

      functionExpr = ProfileOption::FunctionExperience.where(id: profile.function_experiences, default_profile_option: true).pluck(:name).uniq
      functionPref = ProfileOption::FunctionPreference.where(id: profile.function_preferences, default_profile_option: true).pluck(:name).uniq

      countries = Student::COUNTRIES.map { |name, abbr| name if profile.country.to_a.reject(&:empty?).include?(abbr) }.compact

      csv << [
        table.email,

        industryExpr,
        industryPref,
        functionExpr,
        functionPref,

        profile.geographical_preference,
        profile.cities,
        profile.states,
        profile.regions,
        countries,
        profile.international_regions,

      ]
    end
  end
puts csv_data" --app prod-relishcareers > CandidateDetails.csv
