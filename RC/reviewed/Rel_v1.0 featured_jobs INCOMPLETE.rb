# Where does the job description fit? Into the apply_button_text?
# What is the difference between featured_jobs and featured_job_listings?
# How can those 2 tables be linked?

# heroku run bin/qgtunnel bundle exec rails runner
headers = [
    'Company Name', 'Name', 'Slug',
    'Job Level', 'Experience Required', 'Job Type', 'Compensation Currency',
    'Compensation Amount', 'Apply Button Text', 'Apply Button Link',
    'Expiration Date', 'Show Expiration Date', 'Job Phase'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    FeaturedJob.all.each do |table|
      next if table.nil?
      csv << [
        (table.employer_name rescue nil),
        table.name,
        table.slug,
        nil, nil, nil, nil,
        table.job_compensation,
        "Apply Job Here",
        table.application_link,
        nil,
        false,
        table.job_phase
      ]
    end
  end
puts csv_data
# --app prod-relishcareers > RC_featured_jobs_export.csv

# v1
  # create_table "featured_jobs", force: :cascade do |t|
  #   t.string   "name",                    default: "",               null: false
  #   t.string   "description",             default: "About this job", null: false
  #   t.string   "employer_name",           default: "",               null: false
  #   t.string   "location",                default: ""
  #   t.string   "job_phase",               default: "",               null: false
  #   t.string   "application_link",        default: ""
  #   t.jsonb    "employer_contact"
  #   t.string   "job_compensation",        default: ""
  #   t.jsonb    "requirements"
  #   t.string   "date_posted"
  #   t.jsonb    "targeting"
  #   t.datetime "created_at",                                         null: false
  #   t.datetime "updated_at",                                         null: false
  #   t.string   "slug"
  #   t.string   "uuid_key"
  #   t.string   "about_company"
  #   t.string   "application_instruction"
  #   t.boolean  "is_archived",             default: false
  #   t.string   "preview",                 default: "true"
  #   t.boolean  "has_notified_candidates", default: false
  # end

  # create_table "featured_job_listings", force: :cascade do |t|
  #   t.string   "job_title"
  #   t.string   "company_name"
  #   t.string   "city"
  #   t.string   "state"
  #   t.string   "country"
  #   t.string   "formatted_location"
  #   t.string   "source"
  #   t.datetime "posting_date"
  #   t.text     "snippet"
  #   t.text     "url"
  #   t.string   "on_mouse_down"
  #   t.string   "job_key"
  #   t.boolean  "sponsored"
  #   t.boolean  "expired"
  #   t.boolean  "indeed_apply"
  #   t.string   "formatted_location_full"
  #   t.string   "formatted_relative_time"
  #   t.string   "stations"
  #   t.datetime "created_at",              null: false
  #   t.datetime "updated_at",              null: false
  #   t.integer  "company_id"
  #   t.integer  "other_company_id"
  # end


# # v2
#   create_table "featured_jobs", force: :cascade do |t|
#     t.bigint "company_id"
#     t.string "name", null: false
#     t.string "slug"
#     t.datetime "created_at", null: false
#     t.datetime "updated_at", null: false
#     t.string "job_level"
#     t.string "experience_required"
#     t.string "job_type", default: [], array: true
#     t.string "compensation_currency"
#     t.decimal "compensation_amount"
#     t.string "apply_button_text", default: "Apply"
#     t.string "apply_button_link"
#     t.string "expiration_date"
#     t.boolean "show_expiration_date", default: false
#     t.string "job_phase", default: [], array: true
#     t.index ["company_id"], name: "index_featured_jobs_on_company_id"
#     t.index ["name"], name: "index_featured_jobs_on_name"
#   end
