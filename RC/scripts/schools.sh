
heroku run bin/qgtunnel bundle exec rails runner "
  headers = [
    'Name', 'Country', 'States', 'Cities',
    'Degrees Offered', 'Concentrations Offered'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    School.pluck(:name).uniq.each do |name|
      csv << [
        name,
        nil,
        nil,
        nil,
        nil,
        nil
      ]
    end
  end
puts csv_data" --app prod-relishcareers > School.csv

