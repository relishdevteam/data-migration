heroku run bin/qgtunnel bundle exec rails runner "
  headers = [
    'Candidate Email',
    'Graduation Year',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    date_diff = (1.week.ago)..(Date.today.end_of_day)
    Student.where(updated_at: date_diff).each do |table|
      next if table.nil?
      profile = table.student_profile.nil? ? StudentProfile.new : table.student_profile
      next unless profile.graduation_year.to_i == 2023
      csv << [
        table.email,
        profile.graduation_year,
      ]
    end
  end
puts csv_data" --app prod-relishcareers > 2023_Graduation_Year_Candidates.csv
