
# heroku run bin/qgtunnel bundle exec rails runner
  headers = [
    'Years Of Work Experience',
    'USA Work Authorization',
    'Requires Visa Sponsorship',
    'Race',
    'Gender',
    'Ethnicity',
    'Company Size Preference',
    'Visible To Employers',
    'Citizenship',
    'Authorized Work Citizenship'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    User.each do |table|
      next if table.nil?
      csv << [
        nil,
        table.usa_work_authorization,
        table.requires_visa_sponsorship,
        table.race,
        table.gender,
        table.ethnicity,
        nil,
        nil,
        nil,
        nil
    end
  end
puts csv_data
# --app prod-relishcareers > tc_candidate_profile_export.csv

# SELECT DISTINCT users.id, null AS years_of_work_experience, users.usa_work_authorization,
# users.requires_visa_sponsorship, users.race, users.gender, users.ethnicity, null AS company_size_preference,
# users.created_at, users.updated_at, null AS visible_to_employers, null AS citizenship,
# null AS authorized_work_citizenship
# FROM users
# INNER JOIN companies ON users.id = companies.user_id

