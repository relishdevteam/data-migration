heroku run bin/qgtunnel bundle exec rails runner "
  headers = [
    'Candidate Email',

    'School',
    'Degree Category',
    'Graduation Year',
    'Degree Type',
    'Program Type',
    'Degree Level',
    'GMAT Score',
    'Start Year',
    'Gradution Year',

  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Student.find_each do |table|
      next if table.nil?
      profile = table.student_profile.nil? ? StudentProfile.new : table.student_profile

      csv << [
        table.email,

        table&.school&.name,
        profile.degree_category,
        profile.graduation_year,
        profile.degree_type,
        profile.program_type,
        profile.degree_level,
        profile.gmat_score,
        nil,
        profile.graduation_year

      ]
    end
  end
puts csv_data" --app prod-relishcareers > CandidateEducation.csv
