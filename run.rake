### Transparent Careers Files ###
namespace :update do
  namespace :transparent do

    desc 'TC Updated ~ Users'
    task :users do
      system("chmod +x ./Update/tc_scripts/users.sh")
      system("./Update/tc_scripts/users.sh")
    end

    desc 'TC Updated ~ Test Scores'
    task :test_score do
      system("chmod +x ./Update/tc_scripts/test_score.sh")
      system("./Update/tc_scripts/test_score.sh")
    end

    desc 'TC Updated ~ Extracurricular'
    task :extracurricular do
      system("chmod +x ./Update/tc_scripts/extracurricular.sh")
      system("./Update/tc_scripts/extracurricular.sh")
    end

    desc 'TC Updated ~ Education'
    task :education do
      system("chmod +x ./Update/tc_scripts/education.sh")
      system("./Update/tc_scripts/education.sh")
    end

    desc 'TC Updated ~ Candidate Profiles'
    task :candidate_profiles do
      system("chmod +x ./Update/tc_scripts/candidate_profiles.sh")
      system("./Update/tc_scripts/candidate_profiles.sh")
    end

  end

  namespace :relish do

    desc 'RC Updated ~ Users'
    task :users do
      system("chmod +x ./Update/rc_scripts/users.sh")
      system("./Update/rc_scripts/users.sh")
    end

    desc 'RC Updated ~ UserResume'
    task :user_resume do
      system("chmod +x ./Update/rc_scripts/user_resume.sh")
      system("./Update/rc_scripts/user_resume.sh")
    end

    desc 'RC Updated ~ Featured Jobs'
    task :featured_jobs do
      system("chmod +x ./Update/rc_scripts/featured_jobs.sh")
      system("./Update/rc_scripts/featured_jobs.sh")
    end

    desc 'RC Updated ~ Candidate Profile'
    task :candidate_profile do
      system("chmod +x ./Update/rc_scripts/candidate_profile.sh")
      system("./Update/rc_scripts/candidate_profile.sh")
    end

    desc 'RC Updated ~ Candidate Education'
    task :candidate_education do
      system("chmod +x ./Update/rc_scripts/candidate_education.sh")
      system("./Update/rc_scripts/candidate_education.sh")
    end

    desc 'RC Updated ~ Candidate Choices'
    task :candidate_choices do
      system("chmod +x ./Update/rc_scripts/candidate_choices.sh")
      system("./Update/rc_scripts/candidate_choices.sh")
    end

    desc 'RC Updated ~ Candidate Choices'
    task :candidates_grad_year_2023 do
      system("chmod +x ./Update/rc_scripts/2023_grad_candidates.sh")
      system("./Update/rc_scripts/2023_grad_candidates.sh")
    end

  end

  desc 'Move all CSV files to /files'
  task :move do
    system("mv -f -v *_Updated__* ./Update/files")
  end
end

namespace :all do
  namespace :transparent do

    desc 'All TC ~ Candidate Employers'
    task :candidate_employers do
      system("chmod +x ./TC/scripts/candidate_employers.sh")
      system("./TC/scripts/candidate_employers.sh")
    end

    desc 'All TC ~ Candidate Profile'
    task :candidate_profiles do
      system("chmod +x ./TC/scripts/candidate_profiles.sh")
      system("./TC/scripts/candidate_profiles.sh")
    end

    desc 'All TC ~ Candidate Choices'
    task :candidate_choices do
      system("chmod +x ./TC/scripts/candidate_choices.sh")
      system("./TC/scripts/candidate_choices.sh")
    end

    desc 'All TC ~ Company'
    task :companies do
      system("chmod +x ./TC/scripts/companies.sh")
      system("./TC/scripts/companies.sh")
    end

    desc 'All TC ~ Company Quick Links'
    task :company_quick_links do
      system("chmod +x ./TC/scripts/company_quick_links.sh")
      system("./TC/scripts/company_quick_links.sh")
    end

    desc 'All TC ~ Degrees'
    task :degree do
      system("chmod +x ./TC/scripts/degree.sh")
      system("./TC/scripts/degree.sh")
    end

    desc 'All TC ~ Education'
    task :education do
      system("chmod +x ./TC/scripts/education.sh")
      system("./TC/scripts/education.sh")
    end

    desc 'All TC ~ Extracurricular'
    task :extracurricular do
      system("chmod +x ./TC/scripts/extracurricular.sh")
      system("./TC/scripts/extracurricular.sh")
    end

    desc 'All TC ~ Function'
    task :function do
      system("chmod +x ./TC/scripts/function.sh")
      system("./TC/scripts/function.sh")
    end

    desc 'All TC ~ Industry'
    task :industry do
      system("chmod +x ./TC/scripts/industry.sh")
      system("./TC/scripts/industry.sh")
    end

    desc 'All TC ~ Position'
    task :position do
      system("chmod +x ./TC/scripts/position.sh")
      system("./TC/scripts/position.sh")
    end

    desc 'All TC ~ Schools'
    task :schools do
      system("chmod +x ./TC/scripts/schools.sh")
      system("./TC/scripts/schools.sh")
    end

    desc 'All TC ~ Test Score'
    task :test_score do
      system("chmod +x ./TC/scripts/test_score.sh")
      system("./TC/scripts/test_score.sh")
    end

    desc 'All TC ~ Test Type'
    task :test_type do
      system("chmod +x ./TC/scripts/test_type.sh")
      system("./TC/scripts/test_type.sh")
    end

    desc 'All TC ~ Users'
    task :users do
      system("chmod +x ./TC/scripts/users.sh")
      system("./TC/scripts/users.sh")
    end

    desc 'All TC ~ User Resume'
    task :user_resume do
      system("chmod +x ./TC/scripts/user_resume.sh")
      system("./TC/scripts/user_resume.sh")
    end

    desc 'All TC ~ User Profile Pic'
    task :user_profile_pic do
      system("chmod +x ./TC/scripts/user_profile_pic.sh")
      system("./TC/scripts/user_profile_pic.sh")
    end

    desc 'All TC ~ Work Experience'
    task :work_experience do
      system("chmod +x ./TC/scripts/work_experience.sh")
      system("./TC/scripts/work_experience.sh")
    end

  end

  namespace :relish do

    desc 'All RC v1.0 ~ Users'
    task :users do
      system("chmod +x ./RC/scripts/users.sh")
      system("./RC/scripts/users.sh")
    end

    desc 'All RC v1.0 ~ User Resume'
    task :user_resume do
      system("chmod +x ./RC/scripts/user_resume.sh")
      system("./RC/scripts/user_resume.sh")
    end

    desc 'All RC v1.0 ~ User Profile Pic'
    task :user_profile_pic do
      system("chmod +x ./RC/scripts/user_profile_pic.sh")
      system("./RC/scripts/user_profile_pic.sh")
    end

    desc 'All RC v1.0 ~ Schools'
    task :schools do
      system("chmod +x ./RC/scripts/schools.sh")
      system("./RC/scripts/schools.sh")
    end

    desc 'All RC v1.0 ~ Notes'
    task :notes do
      system("chmod +x ./RC/scripts/notes.sh")
      system("./RC/scripts/notes.sh")
    end

    desc 'All RC v1.0 ~ Industry'
    task :industry do
      system("chmod +x ./RC/scripts/industry.sh")
      system("./RC/scripts/industry.sh")
    end

    desc 'All RC v1.0 ~ Function'
    task :function do
      system("chmod +x ./RC/scripts/function.sh")
      system("./RC/scripts/function.sh")
    end

    desc 'All RC v1.0 ~ Featured Jobs'
    task :featured_jobs do
      system("chmod +x ./RC/scripts/featured_jobs.sh")
      system("./RC/scripts/featured_jobs.sh")
    end

    desc 'All RC v1.0 ~ Events'
    task :events do
      system("chmod +x ./RC/scripts/events.sh")
      system("./RC/scripts/events.sh")
    end

    desc 'All RC v1.0 ~ Degrees'
    task :degrees do
      system("chmod +x ./RC/scripts/degrees.sh")
      system("./RC/scripts/degrees.sh")
    end

    desc 'All RC v1.0 ~ Company Tabs'
    task :company_tabs do
      system("chmod +x ./RC/scripts/comp:company_tabs.sh")
      system("./RC/scripts/comp:company_tabs.sh")
    end

    desc 'All RC v1.0 ~ Company Quick Links'
    task :company_quick_links do
      system("chmod +x ./RC/scripts/company_quick_links.sh")
      system("./RC/scripts/company_quick_links.sh")
    end

    desc 'All RC v1.0 ~ Companies'
    task :companies do
      system("chmod +x ./RC/scripts/companies.sh")
      system("./RC/scripts/companies.sh")
    end

    desc 'All RC v1.0 ~ Candidate Profile'
    task :candidate_profile do
      system("chmod +x ./RC/scripts/candidate_profile.sh")
      system("./RC/scripts/candidate_profile.sh")
    end

    desc 'All RC v1.0 ~ Candidate Education'
    task :candidate_education do
      system("chmod +x ./RC/scripts/candidate_education.sh")
      system("./RC/scripts/candidate_education.sh")
    end

    desc 'All RC v1.0 ~ Candidate Choices'
    task :candidate_choices do
      system("chmod +x ./RC/scripts/candidate_choices.sh")
      system("./RC/scripts/candidate_choices.sh")
    end

  end

  desc 'Move all files to RC folder'
  task :move_relish_files do
    system("mv -f -v *.csv ./RC/files")
  end

  desc 'Move all files to TC folder'
  task :move_transparent_files do
    system("mv -f -v *.csv ./TC/files")
  end

end

desc 'Generate all updates'
task :updates => [
  'update:transparent:users',
  'update:transparent:education',
  'update:transparent:test_score',
  'update:transparent:extracurricular',
  'update:transparent:candidate_profiles',

  'update:relish:users',
  'update:relish:user_resume',
  'update:relish:featured_jobs',
  'update:relish:candidate_profile',
  'update:relish:candidate_education',
  'update:relish:candidates_grad_year_2023',

  'update:move'
]

desc 'Generate all RC files'
task :all_rc => [
  'all:relish:users',
  'all:relish:user_resume',
  'all:relish:user_profile_pic',
  'all:relish:schools',
  'all:relish:notes',
  'all:relish:industry',
  'all:relish:function',
  'all:relish:featured_jobs',
  'all:relish:events',
  'all:relish:degrees',
  'all:relish:company_tabs',
  'all:relish:company_quick_links',
  'all:relish:companies',
  'all:relish:candidate_profile',
  'all:relish:candidate_education',
  'all:move_relish_files',
]

desc 'Generate all TC files'
task :all_tc => [
  'all:transparent:candidate_employers',
  'all:transparent:candidate_profiles',
  'all:transparent:candidate_choices',
  'all:transparent:companies',
  'all:transparent:company_quick_links',
  'all:transparent:degree',
  'all:transparent:education',
  'all:transparent:extracurricular',
  'all:transparent:function',
  'all:transparent:industry',
  'all:transparent:position',
  'all:transparent:schools',
  'all:transparent:test_score',
  'all:transparent:test_type',
  'all:transparent:users',
  'all:transparent:user_resume',
  'all:transparent:user_profile_pic',
  'all:transparent:work_experience',
  'all:move_transparent_files',
]

desc 'Genearate all RC and TC new data'
task :all => [
  :all_rc,
  :all_tc
]