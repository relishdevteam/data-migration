# The notes tables seem quite different from v1.0 to v2.0. Is there any overlap?

# heroku run bin/qgtunnel bundle exec rails runner
headers = [
    'Description', 'Saved Employer ID', 'Saved Event ID', 'Saved Featured Job ID',
    'Saved Indeed Job ID',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Note.all.each do |table|
      next if table.nil?
      csv << [
        table.description, nil, nil, nil, nil,
      ]
    end
  end
puts csv_data
# --app prod-relishcareers > rc_notes_export.csv

# # v1
#   create_table "notes", force: :cascade do |t|
#     t.text     "description"
#     t.string   "saveable_type"
#     t.integer  "saveable_id"
#     t.integer  "user_id"
#     t.integer  "save_profile_id"
#     t.datetime "created_at",      null: false
#     t.datetime "updated_at",      null: false
#   end

# #v2
#   create_table "notes", force: :cascade do |t|
#     t.text "description", default: "", null: false
#     t.integer "saved_employer_id"
#     t.integer "saved_event_id"
#     t.integer "saved_featured_job_id"
#     t.integer "saved_indeed_job_id"
#     t.datetime "created_at", precision: 6, null: false
#     t.datetime "updated_at", precision: 6, null: false
#     t.index ["saved_employer_id"], name: "index_notes_on_saved_employer_id"
#     t.index ["saved_event_id"], name: "index_notes_on_saved_event_id"
#     t.index ["saved_featured_job_id"], name: "index_notes_on_saved_featured_job_id"
#     t.index ["saved_indeed_job_id"], name: "index_notes_on_saved_indeed_job_id"
#   end

