# This script joins multiple database tables to create a CSV export

# heroku run bin/qgtunnel bundle exec rails runner
  headers = [
    'Company Name', 'Candidate Email',
    'Compensation Type', 'Currency',
    'Compensation Start Year', 'Currency',
    'Annualized Salary', 'Performance Bonus',
    'Signing Bonus', 'Relocation Bonus',
    'Annual Stock Compensation', 'Misc Compensation',
    'Negotiation Result', 'Hourly Wage',
    'Fixed Compensation Amount', 'Fixed Compensation Interval',
    'Job Duration', 'Equity Granted',
    'Keep Compensation Data Public In Charts'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Company.each do |table|
      next if table.nil?
      csv << [
        table&.company_list&.name,
        company&.user&.email,
        table.compensation_type,
        table.currency,
        table.compensation_start_year,
        table.salary,
        table.performance_bonus,
        table.signing_bonus,
        table.stock_comp,
        table.other_comp,
        table.negotiation_result,
        table.hourly_wage,
        table.fixed_amount,
        table.fixed_type,
        nil,
        table.startup_equity,
        nil
      ]
    end
  end
puts csv_data
# --app prod-relishcareers > tp_position_export.csv
