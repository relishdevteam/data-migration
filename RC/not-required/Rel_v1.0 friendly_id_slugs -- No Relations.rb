# This script joins multiple database tables to create a CSV export

# heroku run bin/qgtunnel bundle exec rails runner
headers = [
    'Slug', 'Sluggable ID', 'Sluggable Type', 'Scope', 'Created At'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Friendly_Id_Slug.all.each do |table|
      next if table.nil?
      csv << [
        table.slug, table.sluggable_id, table.sluggable_type, table.scope, table.created_at
      ]
    end
  end
puts csv_data
# --app prod-relishcareers > tp_position_export.csv

# # v2
#   create_table "friendly_id_slugs", id: :serial, force: :cascade do |t|
#     t.string "slug", null: false
#     t.integer "sluggable_id", null: false
#     t.string "sluggable_type", limit: 50
#     t.string "scope"
#     t.datetime "created_at"
#     t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
#     t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
#     t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id"
#     t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type"
#   end

# # V1
#   create_table "friendly_id_slugs", force: :cascade do |t|
#     t.string   "slug",                      null: false
#     t.integer  "sluggable_id",              null: false
#     t.string   "sluggable_type", limit: 50
#     t.string   "scope"
#     t.datetime "created_at"
#   end