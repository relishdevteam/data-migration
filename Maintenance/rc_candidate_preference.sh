heroku run bin/qgtunnel bundle exec rails runner "
  headers = [
    'Candidate Email',
    'Function Preference',
    'Function Experience',
    'Industry Preference',
    'Industry Experience',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    StudentProfile.find_each do |table|
      next if table.nil?
      functionPref =
        ProfileOption::FunctionPreference
          .where(id: table.function_preferences)
          .pluck(:name)
      csv << [
        table&.student&.email,
        functionPref,
        [],
        [],
        [],
      ]
    end
  end
puts csv_data" --app prod-relishcareers > CandidatePreference_v2.csv
