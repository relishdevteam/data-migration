
# heroku run bin/qgtunnel bundle exec rails runner
  headers = [
    'Company Name', 'URL'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    CompanyList.each do |table|
      next if table.nil?
      csv << [
        table.name,
        table.domain
      ]
    end
  end
puts csv_data
# --app komodo-production > CompanyQuickLinks.csv

# Relish
#   create_table "company_quick_links", force: :cascade do |t|
#     t.integer "company_id", null: false
#     t.string "name", default: "", null: false
#     t.string "url", default: "", null: false
#   end

