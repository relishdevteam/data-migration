heroku run bin/qgtunnel bundle exec rails runner "
  headers = [
    'Candidate Email',
    'Requires Visa Sponsorship',
    'US Work Authorization',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Student.find_each do |table|
      next if table.nil?
      profile =
        table.student_profile.nil? ?
          StudentProfile.new :
          table.student_profile

      csv << [
        table.email,
        profile.requires_visa_sponsorship,
        profile.us_work_authorization,
      ]
    end
  end
puts csv_data" --app prod-relishcareers > CandidateProfile.csv
