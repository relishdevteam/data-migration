heroku run bundle exec rails runner "
  headers = [
    'Email',
    'Profile Pic URL',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    LinkedinInfo.find_each do |table|
      next if table.nil?
      next if table&.user.nil?

      csv << [
        table&.user&.email,
        table&.picture_url
      ]
    end
  end
puts csv_data" --app komodo-production > UserProfilePic.csv
