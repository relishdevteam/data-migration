# heroku run bin/qgtunnel bundle exec rails runner
  headers = [
    'Work Experience ID', 'Title', 'Job Type', 'Visa Sponsorship',
    'Job Function', 'Nearest City', 'Start Month', 'Start Year',
    'End Month', 'End Year', 'Created At', 'Updated At'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Company.all.each do |table|
      next if table.nil?
      position = Position.find_by(id: table.position_id).position
      job_function = Function.find_by(id: table.function_id).name
      created_at = Position.find_by(id: table.position_id).created_at
      updated_at = Position.find_by(id: table.position_id).updated_at
      csv << [
        table.work_experience_id, position, table.job_type, table.visa_sponsorship,
        job_function, nearest_city, table.start_month, table.start_year, nil, nil,
        table.created_at, table.updated_at
      ]
    end
  end
puts csv_data
# --app prod-relishcareers > tp_position_export.csv
