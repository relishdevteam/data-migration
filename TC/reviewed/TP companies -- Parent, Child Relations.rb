# This script joins multiple database tables to create a CSV export, including double joins

# heroku run bundle exec rails runner
  headers = [
    'Name', 'Website',
    'Industries', 'Functions',
    'States', 'Countries',
    'Slug', 'Featured',
    'Company Size', 'Company Type',
    'Archive', 'Positions',
    'Visa Sponsorship', 'Location'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    CompanyList.all.each do |table|
      next if table.nil?

      company_join = Company.where(company_list_id: table.id)

      city_names = City.where(id: company_join.city_id).pluck(:name)
      industry_names = Industry.where(id: company_join.industry_id).pluck(:name)
      function_names = Function.where(id: company_join.function_id).pluck(:name)

      city_join = City.where(id: company_join.city_id).pluck(:name)
      country_names = Country.where(id: city_join.country_id).pluck(:name)

      csv << [
        table.name,
        table.domain,
        industry_names,
        function_names,
        nil,
        country_names,
        table.slug,
        nil,
        nil,
        nil,
        nil,
        nil,
        company_join.map(&:visa_sponsorship),
        city_names
      ]
    end
  end
puts csv_data
# --app komodo-production > Company.csv
