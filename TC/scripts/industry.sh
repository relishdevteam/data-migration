heroku run bundle exec rails runner "
  headers = [
    'Name', 'Description',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Industry.find_each do |table|
      next if table.nil?
      csv << [
        table.name,
        table.description
      ]
    end
  end
puts csv_data" --app komodo-production > Industry.csv

