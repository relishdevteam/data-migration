# This script joins multiple database tables to create a CSV export

# heroku run bundle exec rails runner
  headers = [
    'Company Name', 'Title',
    'Job Type', 'Visa Sponsorship',
    'Job Function', 'Nearest City',
    'Start Month', 'Start Year',
    'End Month', 'End Year',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Company.all.each do |table|
      next if table.nil?
      csv << [
        table&.company_list&.name,
        table&.position&.name,
        table.job_type,
        table.visa_sponsorship,
        table&.function&.name,
        table&.city&.name,
        table.start_month,
        table.start_year,
        nil, nil,
      ]
    end
  end
puts csv_data
# --app komodo-production > WorkPosition.csv
