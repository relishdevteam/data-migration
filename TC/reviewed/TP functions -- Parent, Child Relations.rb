# This script joins multiple database tables to create a CSV export

# heroku run bundle exec rails runner
  headers = [
    'Name', 'Description'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Function.all.each do |table|
      next if table.nil?
      csv << [
        table.name,
        table.description,
      ]
    end
  end
puts csv_data
# --app komodo-production > tc_functions_export.csv
