heroku run bundle exec rails runner "
  headers = [
    'Candidate Email',
    'Position',
    'Type',
    'Organization',
    'Hours Per Week',
    'Start Year',
    'End Year',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Leadership.find_each do |table|
      next if table.nil?
      csv << [
        table&.user&.email,
        table.position_name,
        table.type_name,
        table.organization_name,
        table.hours_per_week,
        table.start_year,
        table.end_year
      ]
    end
  end
puts csv_data" --app komodo-production > Extracurricular.csv
