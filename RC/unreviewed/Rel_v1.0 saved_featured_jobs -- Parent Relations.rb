
# heroku run bin/qgtunnel bundle exec rails runner
headers = [
    'Candidate ID', 'Featured Job ID'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Saved_Featured_Job_Listing.all.each do |table|
      next if table.nil?
      csv << [
        table.user_id, table.featured_job_listing_id
      ]
    end
  end
puts csv_data
# --app prod-relishcareers > rc_saved_featured_jobs_export.csv

# # v1
#   create_table "saved_featured_job_listings", force: :cascade do |t|
#     t.integer  "user_id",                 null: false
#     t.integer  "featured_job_listing_id", null: false
#     t.datetime "created_at",              null: false
#     t.datetime "updated_at",              null: false
#   end

# # v2
#   create_table "saved_featured_jobs", force: :cascade do |t|
#     t.integer "candidate_id", null: false
#     t.integer "featured_job_id", null: false
#     t.index ["candidate_id"], name: "index_saved_featured_jobs_on_candidate_id"
#     t.index ["featured_job_id"], name: "index_saved_featured_jobs_on_featured_job_id"
#   end