# heroku run bin/qgtunnel bundle exec rails runner
  headers = [
    'Name', 'Country', 'States', 'Cities',
    'Degrees Offered', 'Concentrations Offered',
    'Created At', 'Updated At'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    School.all.each do |table|
      next if table.nil?
      csv << [
        table.name,
        nil,
        [],
        [],
        [],
        []
      ]
    end
  end
puts csv_data
# --app komodo-production > tc_school_export.csv
