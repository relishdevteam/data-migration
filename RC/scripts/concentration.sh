heroku run bin/qgtunnel bundle exec rails runner "
  headers = [
    'Name'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    ProfileOption::UndergraduateConcentration.find_each do |table|
      next if table.nil?
      csv << [
        table.name
      ]
    end
  end
puts csv_data" --app prod-relishcareers > Concentration.csv
