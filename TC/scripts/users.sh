heroku run bundle exec rails runner "
  headers = [
    'First Name',
    'Last Name',
    'Email',
    'Slug',
    'Password Digest',
    'Provider',
    'Reset Password Verification Code',
    'Profile Completion Score',
    'Account Confirmation Code',
    'Account Confirmed',
    'Deactivated',
    'Deactivated At'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    User.find_each do |table|
      next if table.nil?
      csv << [
        table.first_name,
        table.last_name,
        table.email,
        nil,
        nil,
        nil,
        nil,
        table.profile_score,
        nil,
        nil,
        nil
      ]
    end
  end
puts csv_data" --app komodo-production > User.csv
