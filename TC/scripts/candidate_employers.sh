
heroku run bundle exec rails runner "
  headers = [
    'Candidate Email',
    'Employer Name'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    User.find_each do |table|
      next if table.nil?
      user_companies =
        table.companies&.map(&:company_list)&.pluck(:name).uniq
      user_companies.each do |company_name|
        csv << [
          table.email,
          company_name
        ]
      end
    end
  end
puts csv_data" --app komodo-production > CandidateEmployer.csv
