
# heroku run bin/qgtunnel bundle exec rails runner
headers = [
    'Candidate ID', 'Recruiter ID', 'Notification Type', 'Is Viewed',
    'Notifier ID', 'Notifier Type', 'Created At', 'Updated At'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Notification.all.each do |table|
      next if table.nil?
      csv << [
        table.user_id, nil, nil, table.has_viewed, nil, nil, 
        table.created_at, table.updated_at
      ]
    end
  end
puts csv_data
# --app prod-relishcareers > rc_notifications_export.csv

# #v1.0
#   create_table "notifications", force: :cascade do |t|
#     t.string   "text"
#     t.integer  "user_id"
#     t.integer  "company_id"
#     t.boolean  "has_viewed", default: false
#     t.datetime "created_at",                                   null: false
#     t.datetime "updated_at",                                   null: false
#     t.jsonb    "link",       default: {"url"=>"", "name"=>""}
#   end

# #v2.0
#   create_table "notifications", force: :cascade do |t|
#     t.integer "candidate_id"
#     t.integer "recruiter_id"
#     t.string "notification_type", null: false
#     t.boolean "is_viewed", default: false
#     t.integer "notifier_id", null: false
#     t.string "notifier_type", null: false
#     t.datetime "created_at", null: false
#     t.datetime "updated_at", null: false
#     t.index ["candidate_id"], name: "index_notifications_on_candidate_id"
#     t.index ["notifier_id"], name: "index_notifications_on_notifier_id"
#     t.index ["recruiter_id"], name: "index_notifications_on_recruiter_id"
#   end