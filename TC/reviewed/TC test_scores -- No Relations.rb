# This script joins multiple database tables to create a CSV export

# heroku run bundle exec rails runner
headers = [
    'Candidate Email',
    'Name',
    'Composite Score',
    'Sub Scores',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    TestScore.all.each do |table|
      next if table.nil?
      csv << [
        table&.user&.email,
        table.test_name,
        table.composite_score,
        table.sub_scores,
      ]
    end
  end
puts csv_data
# --app komodo-production > tc_test_score_export.csv

# # Rel
#   create_table "test_scores", force: :cascade do |t|
#     t.integer "candidate_id", null: false
#     t.string "name", null: false
#     t.integer "composite_score"
#     t.json "sub_scores", default: {}, null: false
#     t.datetime "created_at", null: false
#     t.datetime "updated_at", null: false
#   end

#   # TC

#     create_table "test_scores", id: :serial, force: :cascade do |t|
#     t.integer "user_id"
#     t.integer "test_type_id"
#     t.integer "composite_score"
#     t.json "sub_scores"
#     t.datetime "created_at", null: false
#     t.datetime "updated_at", null: false
#     t.string "test_name"
#     t.index ["test_type_id"], name: "index_test_scores_on_test_type_id"
#     t.index ["user_id"], name: "index_test_scores_on_user_id"
#   end
