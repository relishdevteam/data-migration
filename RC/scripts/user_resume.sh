heroku run bin/qgtunnel bundle exec rails runner "
  headers = [
    'Email',
    'Resume URL'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Document::Resume.find_each do |table|
      next if table.nil?
      csv << [
        table&.documentable&.email,
        table&.record&.expiring_url(1.week)
      ]
    end
  end
puts csv_data" --app prod-relishcareers > UserResume.csv
