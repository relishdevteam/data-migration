heroku run bin/qgtunnel bundle exec rails runner "
  headers = [
    'Name', 'Type', 'Abbreviation',
  ]

  data =
    ProfileOption::DegreeType.default_options

  csv_data = CSV.generate do |csv|
    csv << headers
    data.each do |table|
      next if table.nil?
      csv << [
        table.name, nil, nil,
      ]
    end
  end
puts csv_data" --app prod-relishcareers > Degrees.csv
