heroku run bundle exec rails runner "
  headers = [
    'Candidate Email',
    'CoWorker Quality',
    'Advancement',
    'Training Development',
    'Brand Prestige',
    'Benefits Perks',
    'Firm Stability',
    'Balance Flexibility',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    JobValuePreference.find_each do |table|
      next if table.nil?
      csv << [
        table&.user&.email,
        (table.coworker_quality * 100.0).to_i,
        (table.advancement * 100.0).to_i,
        (table.training_development * 100.0).to_i,
        (table.brand_prestige * 100.0).to_i,
        (table.benefits_perks * 100.0).to_i,
        (table.firm_stability * 100.0).to_i,
        (table.balance_flexibility * 100.0).to_i,
      ]
    end
  end
puts csv_data" --app komodo-production > CandidateChoices.csv
