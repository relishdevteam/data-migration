heroku run bundle exec rails runner "
  headers = [
    'Candidate Email',
    'School',
    'Start Year',
    'End Year',
    'Program Type',
    'GPA Score',
    'Degree'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Education.find_each do |table|
      next if table.nil?
      csv << [
        table&.user&.email,
        table&.school&.name,
        table.start_year,
        table.class_year,
        table.program,
        table.gpa,
        table&.degree&.degree_name,
      ]
    end
  end
puts csv_data" --app komodo-production > Education.csv
