# How can there be a 'candidate_id' or 'work_experience_id' when these are computed averages?

# heroku run bin/qgtunnel bundle exec rails runner
headers = [
    'Company Name', 'Candidate Email',
    'Currency',
    'Compensation Type',
    'Compensation Type',
    'Equity Granted',
    'Annualized Salary',
    'Performance Bonus',
    'Signing Bonus', 'Relocation Bonus',
    'Annual Stock Compensation', 'Misc Compensation', 'Overall Satisfaction',
    'Coworker Quality', 'Advancement', 'Training Development', 'Brand Prestige',
    'Firm Stability', 'Average Weekly Hours', 'Average Travel Percentage',
    'Created At', 'Updated At'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Company_Value_Average_Finals.all.each do |table|
      next if table.nil?
      csv << [
        table.company_list_id, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, table.coworker_quality, table.advancement,
        table.training_development, table.brand_prestige, table.benefits_perks, table.balance_flexibility,
        nil, nil, table.created_at, table.updated_at, nil, nil, nil
      ]
    end
  end
puts csv_data
# --app prod-relishcareers > tc_company_computed_details_export.csv

  # create_table "company_computed_details", force: :cascade do |t|
  #   t.integer "company_id", null: false
  #   t.integer "candidate_id", null: false
  #   t.integer "work_experience_id", null: false
  #   t.string "currency"
  #   t.string "compensation_type"
  #   t.integer "equity_granted"
  #   t.float "annualized_salary"
  #   t.float "performance_bonus"
  #   t.float "signing_bonus"
  #   t.float "relocation_bonus"
  #   t.float "annual_stock_compensation"
  #   t.float "misc_compensation"
  #   t.float "negotiation_result"
  #   t.float "hourly_wage"
  #   t.float "fixed_compensation_amount"
  #   t.integer "overall_satisfaction"
  #   t.integer "coworker_quality"
  #   t.integer "advancement"
  #   t.integer "training_development"
  #   t.integer "brand_prestige"
  #   t.integer "benefits_perks"
  #   t.integer "firm_stability"
  #   t.integer "balance_flexibility"
  #   t.integer "average_weekly_hours"
  #   t.integer "average_travel_percentage"
  #   t.datetime "created_at", precision: 6, null: false
  #   t.datetime "updated_at", precision: 6, null: false
  #   t.string "title"
  #   t.string "job_type"
  #   t.string "visa_sponsorship"
  #   t.index ["candidate_id"], name: "index_company_computed_details_on_candidate_id"
  #   t.index ["company_id"], name: "index_company_computed_details_on_company_id"
  #   t.index ["work_experience_id"], name: "index_company_computed_details_on_work_experience_id"
  # end