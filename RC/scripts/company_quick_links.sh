
heroku run bin/qgtunnel bundle exec rails runner "
  headers = [
    'Company Name', 'Website URL', 'Career URL', 'LinkedIn URL',
    'Twitter URL', 'Facebook URL', 'Others'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Company.find_each do |table|
      next if table.nil?
      prof = table.company_profile rescue CompanyProfile.new

      others = table&.quick_links&.pluck_to_hash(:name, :url)

      csv << [
        table.name,
        prof&.website_url,
        prof&.career_url,
        prof&.linkedin_url,
        prof&.twitter_url,
        prof&.facebook_url,
        others
      ]
    end
  end
puts csv_data" --app prod-relishcareers > CompanyQuickLink.csv

