# This script joins multiple database tables to create a CSV export

# heroku run bin/qgtunnel bundle exec rails runner
headers = [
    'Candidate ID', 'Employer Name', 'Created At', 'Updated At'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Student_Profile.all.each do |table|
      next if table.nil?
      csv << [
        table.id, table.employer_name, table.created_at, table.updated_at
      ]
    end
  end
puts csv_data
# --app prod-relishcareers > rc_candidate_employers_export.csv

# # v2
#   create_table "candidate_employers", force: :cascade do |t|
#     t.integer "candidate_id", null: false
#     t.string "employer_name", null: false
#     t.datetime "created_at", null: false
#     t.datetime "updated_at", null: false
#   end

# v1
  # create_table "student_profiles", force: :cascade do |t|
  #   t.integer  "student_id"
  #   t.string   "enrollment_status"
  #   t.boolean  "update_about"
  #   t.datetime "created_at",                                   null: false
  #   t.datetime "updated_at",                                   null: false
  #   t.string   "recruiting_status"
  #   t.integer  "graduation_year"
  #   t.string   "gmat_score"
  #   t.string   "country",                                                   array: true
  #   t.boolean  "us_work_authorization"
  #   t.string   "undergraduate_concentrations",                              array: true
  #   t.string   "industry_experiences",                                      array: true
  #   t.string   "industry_preferences",                                      array: true
  #   t.string   "function_experiences",                                      array: true
  #   t.string   "function_preferences",                                      array: true
  #   t.string   "company_size_preferences",                                  array: true
  #   t.text     "description"
  #   t.string   "cities",                                                    array: true
  #   t.string   "states",                                                    array: true
  #   t.string   "experience"
  #   t.string   "international_regions",        default: [],                 array: true
  #   t.string   "graduations",                  default: [],                 array: true
  #   t.string   "dual_degrees",                 default: [],                 array: true
  #   t.string   "geographical_preference"
  #   t.string   "mba_associations",                                          array: true
  #   t.string   "degree_type"
  #   t.string   "program_type"
  #   t.string   "referral_code"
  #   t.boolean  "applicant",                    default: false
  #   t.string   "schools_applied",              default: [],                 array: true
  #   t.string   "gender"
  #   t.string   "ethnicity"
  #   t.string   "race"
  #   t.string   "gre_quant"
  #   t.string   "languages",                                                 array: true
  #   t.string   "veteran_status"
  #   t.string   "employer_type_preferences",    default: [],                 array: true
  #   t.string   "regions",                      default: [],                 array: true
  #   t.string   "international_countries",      default: [],                 array: true
  #   t.string   "regions_exp",                  default: [],                 array: true
  #   t.string   "states_exp",                   default: [],                 array: true
  #   t.string   "cities_exp",                   default: [],                 array: true
  #   t.string   "international_regions_exp",    default: [],                 array: true
  #   t.string   "international_countries_exp",  default: [],                 array: true
  #   t.string   "portfolio_links",              default: [],                 array: true
  #   t.string   "skills",                       default: [],                 array: true
  #   t.string   "degree_category"
  #   t.string   "degree_level"
  #   t.string   "gre_verbal"
  #   t.integer  "transparent_data_share",       default: 0
  #   t.boolean  "is_profile_visible",           default: true
  #   t.boolean  "requires_visa_sponsorship"
  #   t.jsonb    "top_resume"
  # end

