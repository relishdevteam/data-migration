heroku run bin/qgtunnel bundle exec rails runner "
  headers = [
    'Company Name',
    'Name',
    'Description',
    'Slug',
    'Location',
    'Job Level',
    'Experience Required',
    'Job Type',
    'Compensation Currency',
    'Compensation Amount',
    'Apply Button Text',
    'Apply Button Link',
    'Expiration Date',
    'Show Expiration Date',
    'Job Phase',
    'Requirements - States',
    'Requirements - Cities',
    'Requirements - International Countries'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    FeaturedJob.where(is_archived: false).each do |table|
      next if table.nil?
      csv << [
        (table.employer_name rescue nil),
        table.name,
        table.description,
        table.slug,
        table.location,
        nil,
        (table.employer_contact['job_exp_required'] rescue nil),
        (table.employer_contact['job_type'] rescue nil),
        nil,
        table.job_compensation,
        'Apply Job Here',
        table.application_link,
        nil,
        false,
        table.job_phase,
        (table.requirements['states'] rescue []),
        (table.requirements['cities'] rescue []),
        (table.requirements['international_countries'] rescue []),
      ]
    end
  end
puts csv_data" --app prod-relishcareers > FeaturedJobs.csv
