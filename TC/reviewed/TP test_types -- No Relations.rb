
# heroku run bundle exec rails runner
headers = [
    'Name', 'Minimum Score',
    'Maximum Score', 'Sub Score Names',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    TestType.all.each do |table|
      next if table.nil?
      csv << [
        table.test_name,
        table.min_score,
        table.max_score,
        table.sub_score_names
      ]
    end
  end
puts csv_data
# --app komodo-production > tc_test_type_export.csv

# Rel

#   create_table "test_types", force: :cascade do |t|
#     t.string "name", null: false
#     t.integer "minimum_score"
#     t.integer "maximum_score"
#     t.string "sub_score_names", default: [], null: false, array: true
#     t.datetime "created_at", null: false
#     t.datetime "updated_at", null: false
#   end

# # TC

#   create_table "test_types", id: :serial, force: :cascade do |t|
#     t.string "test_name"
#     t.integer "min_score"
#     t.integer "max_score"
#     t.json "sub_score_names"
#     t.datetime "created_at", null: false
#     t.datetime "updated_at", null: false
#   end