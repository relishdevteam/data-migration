heroku run bundle exec rails runner "
  headers = [
    'Name', 'Country', 'States', 'Cities',
    'Degrees Offered', 'Concentrations Offered',
    'Created At', 'Updated At'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    School.find_each do |table|
      next if table.nil?
      csv << [
        table.name,
        nil,
        nil,
        nil,
        nil,
        nil
      ]
    end
  end
puts csv_data" --app komodo-production > School.csv
