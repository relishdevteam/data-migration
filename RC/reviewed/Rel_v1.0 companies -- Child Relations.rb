# Industry is not an array in v1... does this matter?

# heroku run bin/qgtunnel bundle exec rails runner
headers = [
    'Name', 'Website', 'Industries', 'Functions',
    'States', 'Cities', 'Countries',
    'Slug', 'Featured', 'Archive',
    'Positions', 'Visa Sponsorship', 'Location'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Company.all.each do |table|
      next if table.nil?
      profile = table.company_profile || CompanyProfile.new
      csv << [
        table.name,
        profile.website_url,
        profile.industry,
        nil,
        profile.states,
        profile.cities,
        profile.international_countries,
        table.slug,
        table.is_featured,
        false,
        nil,
        profile.visa_authorization,
        profile.location
      ]
    end
  end
puts csv_data
# --app prod-relishcareers > rc_companies_export.csv

# # v1

  # create_table "companies", force: :cascade do |t|
  #   t.string   "name"
  #   t.string   "email"
  #   t.string   "passcode",                                  null: false
  #   t.datetime "created_at",                                null: false
  #   t.datetime "updated_at",                                null: false
  #   t.boolean  "is_featured",              default: false
  #   t.string   "slug"
  #   t.integer  "allowed_graduation_years", default: [],                  array: true
  #   t.integer  "allowed_networks",         default: [],                  array: true
  #   t.string   "allowed_degree_types",     default: [],                  array: true
  #   t.datetime "last_activity_at"
  #   t.string   "preview",                  default: "true"
  # end

#   create_table "company_profiles", force: :cascade do |t|
#     t.string   "industry",                                          array: true
#     t.string   "location"
#     t.string   "location_type"
#     t.string   "latitude"
#     t.string   "longitude"
#     t.string   "position_available",      default: [],              array: true
#     t.string   "recruiting_presence"
#     t.string   "company_size"
#     t.integer  "company_id"
#     t.integer  "school_id"
#     t.datetime "created_at",                           null: false
#     t.datetime "updated_at",                           null: false
#     t.string   "website_url"
#     t.string   "career_url"
#     t.string   "linkedin_url"
#     t.string   "twitter_url"
#     t.string   "facebook_url"
#     t.text     "description"
#     t.string   "roles_available",         default: [],              array: true
#     t.string   "cities",                  default: [],              array: true
#     t.string   "states",                  default: [],              array: true
#     t.string   "visa_authorization"
#     t.string   "international_regions",   default: [],              array: true
#     t.string   "international_countries", default: [],              array: true
#     t.string   "employer_type"
#     t.integer  "certified_h1b_visas",     default: 0
#   end

# # v2

#   create_table "companies", force: :cascade do |t|
#     t.string "name", null: false
#     t.string "website"
#     t.string "industries", default: [], array: true
#     t.string "functions", default: [], array: true
#     t.string "states", default: [], array: true
#     t.string "cities", default: [], array: true
#     t.string "countries", default: [], array: true
#     t.datetime "created_at", null: false
#     t.datetime "updated_at", null: false
#     t.string "slug"
#     t.boolean "featured", default: false
#     t.string "company_size"
#     t.string "company_type"
#     t.boolean "archive", default: false
#     t.string "positions", default: [], array: true
#     t.string "visa_sponsorships", default: [], array: true
#     t.text "location", default: ""
#     t.index ["slug"], name: "index_companies_on_slug", unique: true
#   end