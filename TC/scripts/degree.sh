heroku run bundle exec rails runner "
  headers = [
    'Name', 'Type', 'Abbreviation',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Degree.find_each do |table|
      next if table.nil?
      csv << [
        table.degree_name,
        table.degree_type,
        table.degree_abbr
      ]
    end
  end
puts csv_data" --app komodo-production > Degree.csv
