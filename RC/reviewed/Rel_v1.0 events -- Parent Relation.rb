# Is organization ID appropriate here? Unless IDs are deleted anyway...
# 'rsvp' in v1.0 is json, but 'Attendance Status' is string. Change to nil if needed

heroku run bin/qgtunnel bundle exec rails runner "
  headers = [
      'Company Name', 'Name',
      'Calendar Date', 'Attendance Status',
      'Slug',
    ]
    csv_data = CSV.generate do |csv|
      csv << headers
      Event.all.each do |table|
        next if table.nil?
        csv << [
          nil,
          table.name,
          table.event_date,
          nil,
          table.slug,
        ]
      end
    end
puts csv_data" --app prod-relishcareers > Relish_v2_Events.csv

# # v1

#   create_table "events", force: :cascade do |t|
#     t.string   "name"
#     t.datetime "event_date"
#     t.integer  "user_id"
#     t.integer  "organization_id"
#     t.string   "organization_type"
#     t.datetime "created_at",                          null: false
#     t.datetime "updated_at",                          null: false
#     t.jsonb    "targeting"
#     t.datetime "end_date"
#     t.string   "event_time"
#     t.string   "location"
#     t.float    "latitude"
#     t.float    "longitude"
#     t.string   "host",              default: [],                   array: true
#     t.text     "description"
#     t.string   "default"
#     t.string   "slug"
#     t.string   "event_url"
#     t.datetime "deadline"
#     t.string   "logo"
#     t.jsonb    "quick_links",       default: {}
#     t.jsonb    "rsvp"
#     t.string   "event_time_zone"
#     t.string   "event_type",        default: [],                   array: true
#     t.string   "preview",           default: "true"
#     t.string   "archived",          default: "false"
#   end


# # v2

#   create_table "events", force: :cascade do |t|
#     t.bigint "company_id"
#     t.string "name", null: false
#     t.string "calendar_date", null: false
#     t.string "attendance_status"
#     t.string "slug"
#     t.datetime "created_at", null: false
#     t.datetime "updated_at", null: false
#     t.index ["company_id"], name: "index_events_on_company_id"
#     t.index ["name"], name: "index_events_on_name"
#     t.index ["slug"], name: "index_events_on_slug"
#   end


