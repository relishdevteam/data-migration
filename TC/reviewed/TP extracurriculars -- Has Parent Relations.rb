# This script joins multiple database tables to create a CSV export

# heroku run bundle exec rails runner
headers = [
    'Candidate Email', 'Position',
    'Type', 'Organization',
    'Hours Per Week',
    'Start Year', 'End Year',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Leaderships.all.each do |table|
      next if table.nil?
      csv << [
        table&.user&.email,
        table.position_name,
        table.type_name,
        table.organization_name,
        table.hours_per_week,
        table.start_year,
        table.end_year
      ]
    end
  end
puts csv_data
# --app komodo-production > tp_extracurriculars_export.csv

#   create_table "extracurriculars", force: :cascade do |t|
#     t.integer "candidate_id", null: false
#     t.string "position", null: false
#     t.string "type", null: false
#     t.string "organization"
#     t.integer "hours_per_week"
#     t.integer "start_year", null: false
#     t.integer "end_year"
#     t.datetime "created_at", null: false
#     t.datetime "updated_at", null: false
#   end

# ## TC Extracurricular Tables

#   create_table "leadership_positions", id: :serial, force: :cascade do |t|
#     t.string "leadership_position"
#     t.datetime "created_at", null: false
#     t.datetime "updated_at", null: false
#     t.index ["leadership_position"], name: "index_leadership_positions_on_leadership_position"
#   end

#   create_table "leaderships", id: :serial, force: :cascade do |t|
#     t.integer "user_id"
#     t.integer "leadership_type_id"
#     t.integer "leadership_position_id"
#     t.string "organization_name"
#     t.datetime "created_at", null: false
#     t.datetime "updated_at", null: false
#     t.integer "hours_per_week"
#     t.integer "start_year"
#     t.integer "end_year"
#     t.string "type_name"
#     t.string "position_name"
#     t.index ["leadership_position_id"], name: "index_leaderships_on_leadership_position_id"
#     t.index ["leadership_type_id"], name: "index_leaderships_on_leadership_type_id"
#     t.index ["user_id"], name: "index_leaderships_on_user_id"
#   end

#   create_table "leadership_types", id: :serial, force: :cascade do |t|
#     t.string "leadership_type"
#     t.datetime "created_at", null: false
#     t.datetime "updated_at", null: false
#     t.index ["leadership_type"], name: "index_leadership_types_on_leadership_type"
#   end