# Results of All data exports

  ## Company
    4,544 records ~ Relish v1.0

    11,835 records ~ Transparent

  ## Company Quick Links
    4,544 records ~ Relish v1.0

    107 records ~ Transparent

  ## Featured Jobs
    555 records ~ Relish v1.0

    N/A records ~ Transparent

  ## Candidate Employer
  ##### Employers whom Candidates have Work Exp

    N/A records ~ Relish v1.0

    36,609 records ~ Transparent

  ## Degree
    72 records ~ Relish v1.0

    191 records ~ Transparent

  ## Education
    65,757 records ~ Relish v1.0

    76,443 records ~ Transparent

  ## Extracurricular
    N/A records ~ Relish v1.0

    8,092 records ~ Transparent

  ## Event
  ##### ~ We don't have data for events yet ~
    0 records ~ Relish v1.0

    0 records ~ Transparent

  ## Function
    47 records ~ Relish v1.0

    33 records ~ Transparent

  ## Industry
    43 records ~ Relish v1.0

    34 records ~ Transparent

  ## Position
    N/A records ~ Relish v1.0

    6,265 records ~ Transparent

  ## School
    1,548 records ~ Relish v1.0

    3,593 records ~ Transparent

  ## User
  ####  ~ Student Type ~
    65,751 records ~ Relish v1.0

    70,439 records ~ Transparent

  ## Candidate Profile
    65,758 records ~ Relish v1.0

    70,440 records ~ Transparent

  ## Candidate Choices
  ##### Match Score fields
    N/A records ~ Relish v1.0

    3,324 records ~ Transparent

  ## User Profile Pic (URLs)
  ##### None of the links in TC work
    10,717 records ~ Relish v1.0

    25,160 records ~ Transparent

  ## User Resume (URLs)
    11,883 records ~ Relish v1.0

    8,403 records ~ Transparent

  ## Test Score
    N/A records ~ Relish v1.0

    12,288 records ~ Transparent

  ## Test Type
    N/A records ~ Relish v1.0

    9 records ~ Transparent

  ## Work Experience
    N/A records ~ Relish v1.0

    36,282 records ~ Transparent