# This script joins multiple database tables to create a CSV export

# heroku run bin/qgtunnel bundle exec rails runner
headers = [
    'Company ID', 'Name', 'Created At', 'Updated At', 'Is Main Tab'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Company_Page_Tab.all.each do |table|
      next if table.nil?
      csv << [
        table.company_id, table.name, table.created_at, table.updated_at, nil
      ]
    end
  end
puts csv_data
# --app prod-relishcareers > rc_company_tabs_export.csv

# # v2
#   create_table "company_tabs", force: :cascade do |t|
#     t.integer "company_id", null: false
#     t.string "name", null: false
#     t.datetime "created_at", precision: 6, null: false
#     t.datetime "updated_at", precision: 6, null: false
#     t.boolean "is_main_tab", default: false
#   end

#   # v1
#     create_table "company_page_tabs", force: :cascade do |t|
#     t.string   "name"
#     t.text     "description"
#     t.integer  "company_page_id"
#     t.datetime "created_at",      null: false
#     t.datetime "updated_at",      null: false
#     t.json     "targeting"
#   end