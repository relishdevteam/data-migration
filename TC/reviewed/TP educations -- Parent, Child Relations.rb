# This script joins multiple database tables to create a CSV export

# heroku run bundle exec rails runner
headers = [
    'School', 'Start Year',
    'End Year', 'Program Type',
    'Concentration', 'Candidate Email',
    'GPA Score'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Education.all.each do |table|
      next if table.nil?
      csv << [
        table&.school&.name,
        table.start_year,
        table.class_year,
        table.program,
        table&.major&.name,
        table&.user&.email,
        table.gpa
      ]
    end
  end
puts csv_data
# --app komodo-production > tc_educations_export.csv

# SELECT DISTINCT schools.name AS school, educations.start_year AS start_year,
# educations.class_year AS end_year, educations.program AS program_type, majors.name AS concentration,
# users.id AS candidate_id, educations.created_at AS created_at, educations.updated_at AS updated_at,
# educations.gpa AS gpa_score
# FROM users
# INNER JOIN educations ON educations.user_id = users.id
# INNER JOIN degrees on degrees.id = educations.degree_id
# INNER JOIN companies ON users.id = companies.user_id
# INNER JOIN majors ON majors.id = educations.major_id
# INNER JOIN schools on schools.id = educations.school_id
