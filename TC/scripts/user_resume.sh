heroku run bundle exec rails runner "
  headers = [
    'Email',
    'Resume URL',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Resume.find_each do |table|
      next if table.nil?
      next if table&.user.nil?

      resume_url = ResumeManager.new(table&.user, table&.file_name).download_url(expire_seconds: 1.week.seconds.to_i)
      csv << [
        table&.user&.email,
        resume_url
      ]
    end
  end
puts csv_data" --app komodo-production > UserResume.csv
