# Why no preferences, candidate ID in v1?
# What is searchable ID? I do not see a parent table

# heroku run bin/qgtunnel bundle exec rails runner
headers = [
    'Preferences', 'Candidate ID', 'Search Page', 'Title', 'Created At', 'Updated At'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Search.all.each do |table|
      next if table.nil?

      csv << [
        nil, nil, table.url, table.name, table.created_at, table.updated_at
      ]
    end
  end
puts csv_data
# --app prod-relishcareers > tp_position_export.csv

# # v2
#   create_table "saved_searches", force: :cascade do |t|
#     t.jsonb "preferences"
#     t.integer "candidate_id", null: false
#     t.string "search_page", null: false
#     t.string "title", null: false
#     t.datetime "created_at", precision: 6, null: false
#     t.datetime "updated_at", precision: 6, null: false
#     t.index ["preferences"], name: "index_saved_searches_on_preferences"
#   end

# # v1
#   create_table "searches", force: :cascade do |t|
#     t.string   "name"
#     t.text     "url"
#     t.integer  "searchable_id"
#     t.string   "searchable_type"
#     t.datetime "created_at",      null: false
#     t.datetime "updated_at",      null: false
#   end