# heroku run bundle exec rails runner
  headers = [
    'Name', 'Description',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Industry.all.each do |table|
      next if table.nil?
      csv << [
        table.name,
        table.description
      ]
    end
  end
puts csv_data
# --app komodo-production > tc_industries_export.csv

