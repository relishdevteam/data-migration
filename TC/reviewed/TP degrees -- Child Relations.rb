
# heroku run bundle exec rails runner
headers = [
    'Name', 'Type', 'Abbreviation',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Degree.all.each do |table|
      next if table.nil?
      csv << [
        table.degree_name,
        table.degree_type,
        table.degree_abbr
      ]
    end
  end
puts csv_data
# --app komodo-production > tc_degree_export.csv

# SELECT degree_name AS name, degree_type AS type, degree_abbr AS abbreviation, created_at AS created_at, updated_at AS updated_at
# FROM degrees

