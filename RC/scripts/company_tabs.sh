heroku run bin/qgtunnel bundle exec rails runner "
  headers = [
    'Company Name',

    'About Us Title',
    'About Us Content',

    'Events',
    'Videos',
    'Photos',
    'Documents',

    'Other'

  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    CompanyPage.find_each do |table|
      company = table.company
      next if table.nil?
      next if company.nil?

      videos = table.videos.pluck_to_hash(:title, :url, :description)
      photos = table.company_pics.map { |s| s.avatar.url }
      documents = table.documents.map { |s| s.record.url }
      other = table.company_page_tabs.pluck_to_hash(:name, :description)

      csv << [
        company.name,

        table.about_us,
        table.about_company,

        [],
        videos,
        photos,
        documents,

        other,
      ]
    end
  end
puts csv_data" --app prod-relishcareers > CompanyTabs.csv
