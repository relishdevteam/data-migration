heroku run bin/qgtunnel bundle exec rails runner "
  headers = [
    'Name',
    'Website',
    'Industries',
    'Functions',
    'States',
    'Cities',
    'Countries',
    'Slug',
    'Featured',
    'Archive',
    'Positions',
    'Visa Sponsorship',
    'Location',
    'Company Size',
    'Company Type',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    Company.find_each do |table|
      next if table.nil?
      profile = table.company_profile || CompanyProfile.new
      countries = Student::COUNTRIES.map { |name, abbr| name if profile.international_countries.reject(&:empty?).include?(abbr) }.compact
      csv << [
        table.name,
        profile.website_url,
        profile.industry.reject(&:empty?),
        nil,
        profile.states.reject(&:empty?),
        profile.cities.reject(&:empty?),
        countries,
        table.slug,
        table.is_featured,
        false,
        nil,
        nil,
        profile.location,
        profile.company_size,
        profile.employer_type,
      ]
    end
  end
puts csv_data" --app prod-relishcareers > Companies.csv
