heroku run bin/qgtunnel bundle exec rails runner "
  headers = [
    'Email',
    'Require Visa Sponsorrship',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    StudentProfile.find_each do |table|
      next if table.nil?
      csv << [
        table&.student&.email,
        table&.requires_visa_sponsorship,
      ]
    end
  end
puts csv_data" --app prod-relishcareers > RC_Require_Visa_Authorization.csv
