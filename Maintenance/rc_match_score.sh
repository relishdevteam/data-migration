# This script joins multiple database tables to create a CSV export, including double joins

heroku run bundle exec rails runner "
  headers = [
    'Company Name',
    'Match Score value',
    'User - Coworker Quality',
    'User - Advancement',
    'User - Training Development',
    'User - Brand Prestige',
    'User - Benefits Perks',
    'User - Firm Stability',
    'User - Balance Flexibility',
  ]

  user = User.find_by(email: 'marvin@relishcandidate.com')
  values = user.candidate_values.attributes.to_options

  csv_data = CSV.generate do |csv|
    csv << headers
    Company.find_each do |company|
      result = MatchScoreCalculator.new(values).calculate_for_company(company)
      next if result.nil?
      csv << [
        company&.name,
        result,
        values[:coworker_quality],
        values[:advancement],
        values[:training_development],
        values[:brand_prestige],
        values[:benefits_perks],
        values[:firm_stability],
        values[:balance_flexibility],
      ]
    end
  end
puts csv_data" --app staging-relishcareers > Staging_RC_User_MatchScore.csv

# MatchScoreCalculator.new(user.candidate_values.attributes.to_options).calculate_for_company(company)