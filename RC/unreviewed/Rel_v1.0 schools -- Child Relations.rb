# I have received a file with university locations that can be joined to existing data
# However I need time to work on it

# heroku run bin/qgtunnel bundle exec rails runner
headers = [
    'Name', 'Country', 'States', 'Cities',
    'Degrees Offered', 'Concentrations Offered', 
    'Created At', 'Updated At'
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    School.all.each do |table|
      next if table.nil?
      csv << [
        table.name, nil, nil, nil, nil, nil, table.created_at, table.updated_at
      ]
    end
  end
puts csv_data
# --app prod-relishcareers > tp_position_export.csv

# #v2
#   create_table "schools", force: :cascade do |t|
#     t.string "name", null: false
#     t.string "country"
#     t.string "states", default: [], array: true
#     t.string "cities", default: [], array: true
#     t.string "degrees_offered", default: [], array: true
#     t.string "concentrations_offered", default: [], array: true
#     t.datetime "created_at", null: false
#     t.datetime "updated_at", null: false
#   end

#   #v1

#   create_table "school_profiles", force: :cascade do |t|
#     t.text     "address"
#     t.text     "description"
#     t.integer  "school_id"
#     t.datetime "created_at",   null: false
#     t.datetime "updated_at",   null: false
#     t.string   "university"
#     t.string   "location"
#     t.integer  "size"
#     t.string   "website_url"
#     t.string   "facebook_url"
#     t.string   "twitter_url"
#     t.string   "linkedin_url"
#     t.string   "shortname"
#   end

#   create_table "schools", force: :cascade do |t|
#     t.string   "name"
#     t.string   "email"
#     t.string   "passcode",   null: false
#     t.datetime "created_at", null: false
#     t.datetime "updated_at", null: false
#   end
