# Do we really not have specific degree names in v1.0, othan than MBA, etc?

# heroku run bin/qgtunnel bundle exec rails runner
headers = [
    'Name', 'Type', 'Abbreviation',
  ]
  csv_data = CSV.generate do |csv|
    csv << headers
    ProfileOption::DegreeType.all.each do |table|
      next if table.nil?
      csv << [
        table.name, nil, nil,
      ]
    end
  end
puts csv_data
# --app prod-relishcareers > rc_degrees_export.csv

# # v2
#   create_table "degrees", force: :cascade do |t|
#     t.string "name", null: false
#     t.string "type", null: false
#     t.string "abbreviation"
#     t.datetime "created_at", null: false
#     t.datetime "updated_at", null: false
#   end

# # v1
#   create_table "degree_fields", force: :cascade do |t|
#     t.string   "name"
#     t.string   "default_options", default: [],                 array: true
#     t.boolean  "multiple",        default: false
#     t.datetime "created_at",                      null: false
#     t.datetime "updated_at",                      null: false
#   end
